window.app = angular.module("app", ["ngRoute", "ngAnimate"])
  .config(["$routeProvider", function ($routeProvider) {
    "use strict";
    $routeProvider
      .when("/", {
        templateUrl: "./partials/home.html",
        controller:"HomeCtrl"
      })
      .otherwise({
        redirectTo: "/"
      });
}]);